from piperci.faas.exceptions import PiperDelegateError


def handle(request, task, config):
    """
    Entrypoint to the pythonwheel FaaS.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this faas
    :return: (response_body, code)
    """

    # See piperci.faas.this_task for gman.task interface usage docs

    my_executor_uri = config['executor_url'].replace(endpoint=config['endpoint'])

    try:
        task.info('Pythonwheel faas gateway handler.handle called successfully')
    except PiperDelegateError as e:
        return {'error': f'{str(e)}: no delegate was attempted'}, 400

    try:
        task.delegate(my_executor_uri, task.task)
        return task.complete('Pythonwheel gateway completed successfully')
    except PiperDelegateError as e:
        return {'error': f'{str(e)}'}, 400
