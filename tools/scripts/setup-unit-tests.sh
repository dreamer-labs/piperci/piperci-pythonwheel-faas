#!/bin/bash

FUNCTION_TEMPDIR=$1
FUNCTION_TEMPLATE=$2

THIS_SCRIPT=$(basename "${BASH_SOURCE[0]}")
THIS_RELDIR=$(dirname "${BASH_SOURCE[0]}")
REPO_BASEDIR=$(cd $THIS_RELDIR; cd ../../; pwd)

[[ -d ${FUNCTION_TEMPDIR} ]] && rm -r ${FUNCTION_TEMPDIR};

mkdir ${FUNCTION_TEMPDIR}

ls -la ${FUNCTION_TEMPDIR}

export REPO_BASEDIR
for function_data in $(python - <<DOC
import yaml
import os

def main():
    repo_dir = os.path.realpath(os.environ.get('REPO_BASEDIR', './'))
    stack_yml = f'{repo_dir}/stack.yml'
    with open(stack_yml, 'r') as stack_yml:
        stack = yaml.safe_load(stack_yml)
        for function, data in stack['functions'].items():
          print(f"{data['handler']} {data['lang']}")

if __name__ == '__main__':
  main()

DOC);
do
  function_path="${function_data% *}"
  function_name=$(basename $function_path)
  function_lang="${function_data#* }"
  echo $function_path
  cp -r ${FUNCTION_TEMPLATE}/function ${FUNCTION_TEMPDIR}/${function_name}
  cp -r ${function_path}/*.py ${FUNCTION_TEMPDIR}/${function_name}
done
