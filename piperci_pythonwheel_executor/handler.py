from piperci.faas.exceptions import PiperError


def handle(request, task, config):
    """
    Entrypoint to the pythonwheel FaaS async executor function.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this faas
    :return: (response_body, code)
    """

    artifact = f'pythonwheel artifact content {task.task["task_id"]}'
    artfile_path = f'test_artifact_{task.task["task_id"]}'
    # See piperci.faas.this_task for gman.task interface usage docs
    try:
        task.info('Pythonwheel faas gateway handler.handle called successfully')

        with open(f'test_artifact_{task.task["task_id"]}', 'w') as artfile:
            artfile.write(artifact)

        task.artifact(artfile_path)

        task.complete("Pythonwheel FaaS doesn't do anything but test a pythonwheel exec path")
    except PiperError:
        pass
